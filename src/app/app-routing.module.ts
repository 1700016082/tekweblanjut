import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { AdminComponent } from './pages/admin/admin.component';
import { KonsumenComponents } from './pages/konsumen/konsumen.component';
import { SupplierComponent } from './pages/supplier/supplier.component';
import { LoginComponent } from './pages/login/login.component';


const routes: Routes = [
{
path:'home',
component:HomeComponent
},
{
path:'admin',
component:AdminComponent
},
{
path:'konsumen',
component:KonsumenComponents
},
{
path:'supplier',
component:SupplierComponent
},
{
path:'login',
component:LoginComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
