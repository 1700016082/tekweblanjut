import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KonsumenComponent } from './konsumen.component';

describe('KonsumenComponent', () => {
  let component: KonsumenComponent;
  let fixture: ComponentFixture<KonsumenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KonsumenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KonsumenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
